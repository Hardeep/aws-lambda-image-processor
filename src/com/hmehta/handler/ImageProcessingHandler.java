package com.hmehta.handler;

import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.awt.Graphics2D;
import javax.imageio.ImageIO;
import java.util.regex.Matcher;
import java.awt.RenderingHints;
import java.util.regex.Pattern;
import com.hmehta.util.Constants;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import com.amazonaws.services.s3.AmazonS3;
import com.hmehta.model.ImageProcessRequest;
import com.hmehta.model.ImageProcessResponse;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.lambda.runtime.RequestHandler;

/**
 * Image Processing Lambda function
 * 
 * @author hardeep
 *
 */
public class ImageProcessingHandler implements RequestHandler<ImageProcessRequest, ImageProcessResponse> {

	private final String JPG_TYPE = (String) "jpg";
	private final String JPG_MIME = (String) "image/jpeg";
	private final String PNG_TYPE = (String) "png";
	private final String PNG_MIME = (String) "image/png";

	@Override
	public ImageProcessResponse handleRequest(ImageProcessRequest request, Context context) {
		ImageProcessResponse response = new ImageProcessResponse();
		context.getLogger().log("Function=" + context.getFunctionName() + ", " + request.getClass() + "=" + request.toString());

		try {
			if (request != null) {
				String srcBucket = "com.hmehta.images";
				String dstBucket = srcBucket + ".processed";
				String srcKey = request.getImageName();
				String dstKey = "processed-" + srcKey;

				// Validate that source and destination buckets are different
				if (srcBucket.equals(dstBucket)) {
					context.getLogger().log("Destination bucket must not match source bucket.");
					return response;
				}

				// Infer the image type.
				Matcher matcher = Pattern.compile(".*\\.([^\\.]*)").matcher(srcKey);
				if (!matcher.matches()) {
					context.getLogger().log("Unable to infer image type for key " + srcKey);
					return response;
				}

				String imageType = matcher.group(1);
				if (!(JPG_TYPE.equals(imageType)) && !(PNG_TYPE.equals(imageType))) {
					context.getLogger().log("Skipping non-image " + srcKey);
					return response;
				}

				// Download the image from S3 into a stream
				AmazonS3 s3Client = new AmazonS3Client();
				S3Object s3Object = s3Client.getObject(new GetObjectRequest(srcBucket, srcKey));
				InputStream objectData = s3Object.getObjectContent();

				// Read the source image
				BufferedImage srcImage = ImageIO.read(objectData);
				int srcHeight = srcImage.getHeight();
				int srcWidth = srcImage.getWidth();
				// Infer the scaling factor to avoid stretching the image
				// unnaturally
				float scalingFactor = Math.min(Float.parseFloat(request.getImageWidth()) / srcWidth,
						Float.parseFloat(request.getImageWidth()) / srcHeight);
				int width = (int) (scalingFactor * srcWidth);
				int height = (int) (scalingFactor * srcHeight);

				BufferedImage resizedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
				if (request.getImageColorMode().equals(Constants.BLACK_AND_WHITE)) {
					resizedImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_BINARY);
				} else if (request.getImageColorMode().equals(Constants.MONOCHROME)) {
					resizedImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
				}

				Graphics2D g = resizedImage.createGraphics();
				// Fill with white before applying semi-transparent (alpha)
				// images
				g.setPaint(Color.white);
				g.fillRect(0, 0, width, height);
				// Simple bilinear resize
				// If you want higher quality algorithms, check this link:
				// https://today.java.net/pub/a/today/2007/04/03/perils-of-image-getscaledinstance.html
				g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
				g.drawImage(srcImage, 0, 0, width, height, null);
				g.dispose();

				// Re-encode image to target format
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				ImageIO.write(resizedImage, imageType, os);
				InputStream is = new ByteArrayInputStream(os.toByteArray());
				// Set Content-Length and Content-Type
				ObjectMetadata meta = new ObjectMetadata();
				meta.setContentLength(os.size());
				if (JPG_TYPE.equals(imageType)) {
					meta.setContentType(JPG_MIME);
				}
				if (PNG_TYPE.equals(imageType)) {
					meta.setContentType(PNG_MIME);
				}

				// Uploading to S3 destination bucket
				context.getLogger().log("Writing to: " + dstBucket + "/" + dstKey);
				s3Client.putObject(dstBucket, dstKey, is, meta);
				response.setProcessedImageURL("https://s3.amazonaws.com/" + dstBucket + "/" + dstKey);
				context.getLogger().log("Successfully processed " + srcBucket + "/" + srcKey + " and uploaded to " + response.getProcessedImageURL());
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return response;
	}
}