package com.hmehta.model;

public class ImageProcessResponse {

	String processedImagURL;

	public ImageProcessResponse() {

	}

	public String getProcessedImageURL() {
		return processedImagURL;
	}

	public void setProcessedImageURL(String processedImagURL) {
		this.processedImagURL = processedImagURL;
	}

}