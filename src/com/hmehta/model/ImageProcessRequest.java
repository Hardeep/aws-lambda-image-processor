package com.hmehta.model;

import com.hmehta.util.Constants;

public class ImageProcessRequest {

	private String imageName = null;
	private String imageWidth = null;
	private String imageColorMode = Constants.COLOR;

	public ImageProcessRequest() {

	}

	public ImageProcessRequest(String imageName, String imageWidth, String imageColorMode) {
		this.imageName = imageName;
		this.imageWidth = imageWidth;
		this.imageColorMode = imageColorMode;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImageWidth() {
		return imageWidth;
	}

	public void setImageWidth(String imageWidth) {
		this.imageWidth = imageWidth;
	}

	public String getImageColorMode() {
		return imageColorMode;
	}

	public void setImageColorMode(String imageColorMode) {
		this.imageColorMode = imageColorMode;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ImageProcessRequest {imageName=").append(imageName).append(", imageWidth=").append(imageWidth).append(", imageColorMode=")
				.append(imageColorMode).append("}");
		return builder.toString();
	}

}