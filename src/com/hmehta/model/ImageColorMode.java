package com.hmehta.model;

public enum ImageColorMode {

	COLOR, MONOCHROME, BLACK_AND_WHITE

}