package com.hmehta;

import org.junit.Test;
import java.io.IOException;
import org.junit.BeforeClass;
import com.hmehta.util.Constants;
import com.hmehta.handler.ImageProcessingHandler;
import com.hmehta.model.ImageProcessRequest;
import com.hmehta.model.ImageProcessResponse;
import com.amazonaws.services.lambda.runtime.Context;

/**
 * A simple test harness for locally invoking your Lambda function handler.
 */
public class ImageProcessingHandlerTest {

	// private static Object input;

	@BeforeClass
	public static void createInput() throws IOException {
		// input = null;
	}

	private Context createContext() {
		TestContext ctx = new TestContext();
		ctx.setFunctionName("ImageProcessingHandler.handleRequest");
		return ctx;
	}

	@Test
	public void testImageProcessingHandler() {
		ImageProcessingHandler handler = new ImageProcessingHandler();
		Context context = createContext();

		ImageProcessRequest request = new ImageProcessRequest("1466463997769.jpg", "700", Constants.MONOCHROME);
		ImageProcessResponse response = handler.handleRequest(request, context);

		if (response != null) {
			context.getLogger().log("Function=" + context.getFunctionName() + ", ProcessedImageURL=" + response.getProcessedImageURL());
		}
	}
}
